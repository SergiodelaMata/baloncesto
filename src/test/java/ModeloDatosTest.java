import org.junit.jupiter.api.Test;

import Entities.Jugador;
import Model.ModeloDatos;

import static org.junit.jupiter.api.Assertions.*;
public class ModeloDatosTest {

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Carroll";
        ModeloDatos instance = new ModeloDatos();
        Jugador playerInitial = instance.getJugador(nombre);
        instance.actualizarJugador(nombre);
        Jugador playerFinal = instance.getJugador(nombre);
        assertEquals(playerInitial.getVotos() + 1, playerFinal.getVotos());
    }
}