package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.ModeloDatos;

@WebServlet("/PLA_01_SetVotesToCero")
public class PLA_01_SetVotesToCero extends HttpServlet{

    public PLA_01_SetVotesToCero() {
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();

		try 
		{
			// ES: Sincronizar la base de datos
			// EN: Sincronize the database
			boolean ok = ModeloDatos.setVotesToCero();
			if(ok)
			{
				out.print("Se han puesto todos los votos de los jugadores a 0.");
			}else
			{
				out.print("No se han puesto todos los votos de los jugadores a 0.");
			}
			
		} catch (NumberFormatException nfe) 
		{
			out.println("Number Format Exception" + nfe);
		} catch (IndexOutOfBoundsException iobe) 
		{
			out.println("Index out of bounds Exception" + iobe);
		} finally 
		{
			out.close();
		}
	}

}
