function setVotesToCero()
{
    $.ajax({
        url: parent.getAdapter('PLA_01_SetVotesToCero'),
        type: 'POST',
        data: {},
        async: false,
        error : function(jqXHR, textStatus, errorThrown) 
        {
            alert("No se ha podido realizar la operación.");
        },
        success : function(response)
        {
            alert(response);
        }
    });
}

function seeVotes()
{
    window.location.href = "VerVotos.jsp";
}