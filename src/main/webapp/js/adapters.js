var listadoadapters  = {
    PLA_01_SetVotesToCero : "PLA_01_SetVotesToCero",
};

//Función para recoger el listado de adapters
function getAdapters()
{
    return listadoadapters;
}

//Función para recoger el valor de un adapter en concreto
function getAdapter(adapter)
{
    var nombreadapter = "/Baloncesto/" + listadoadapters[adapter];

    return nombreadapter;
}