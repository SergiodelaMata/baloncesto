<%@ page import="Entities.Jugador"%>
<%@ page import="Model.ModeloDatos"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE> 
<html lang="es">
    <head><title>Ver votos</title></head>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
    rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous"
    />
    <link
    rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
    integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
    crossorigin="anonymous"
    />

    <body class="resultado">
        <p style="font-size: 40;">
            Resultados hasta el momento de la votación al mejor jugador de la liga ACB
        </p>
        <hr>
        <div class="col-6 my-5">
            <%
                ArrayList<Jugador> listPlayers = ModeloDatos.getListJugadores();
                Iterator<Jugador> iterPlayers = listPlayers.iterator();
            %>
            <table class="table table-dark" id="tableVotes">
                <thead>
                    <th scope="col"></th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Nº Votos</th>
                </thead>
                <tbody>
                    <%
                        int counter = 0;
                        Jugador player = new Jugador();
                        while(iterPlayers.hasNext()){
                            counter++;
                            player = iterPlayers.next();
                        
                    %>
                    <tr>
                        <td id="counter<%=counter%>"><%=counter%></td>                    
                        <td id="player<%=counter%>"><%=player.getNombre()%></td>
                        <td id="votes<%=counter%>"><%=player.getVotos()%></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </div>
        <br>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
